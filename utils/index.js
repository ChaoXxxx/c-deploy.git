const fs = require('fs')
const path = require('path')
const chalk = require('chalk')

const defaultLog = (log) => console.log(chalk.blue(`---------------- ${log} ----------------`))
const errorLog = (log) => console.log(chalk.red(`---------------- ${log} ----------------`))
const successLog = (log) => console.log(chalk.green(`---------------- ${log} ----------------`))

const deployFile = `${process.cwd()}/deploy.config.js`
const hasConfigFile = () => {
  if (fs.existsSync(deployFile)) {
    return true
  } else {
    return false
  }
}

const clearDistZip = (config) => {
  return new Promise((resolve, reject) => {
    const distZipPath = path.resolve(process.cwd(), `${config.publicPath}.zip`)
    fs.unlink(distZipPath, (err) => {
      if (err) {
        reject('本地zip文件删除失败...')
      }
      resolve()
    })
  })
}

module.exports = {
  defaultLog,
  errorLog,
  successLog,
  hasConfigFile,
  clearDistZip
}
