const fs = require('fs')
const path = require('path')
const { errorLog, successLog, hasConfigFile } = require('../utils')
const configPath = `${process.cwd()}/deploy.config.js`

const init = () => {
  if (hasConfigFile()) {
    errorLog("deploy.config.js 已在根目录")
  } else {
    const deployFile = path.resolve(__dirname, '../config/deploy.config.js')
    fs.writeFileSync(configPath, fs.readFileSync(deployFile))
    successLog('配置生成成功')
  }
}

module.exports = init
