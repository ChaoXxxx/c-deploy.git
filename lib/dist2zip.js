const path = require('path')
const compressing = require('compressing')

const { clearDistZip, defaultLog, successLog, errorLog } = require('../utils')

const dist2zip = async (config) => {
  defaultLog('项目开始压缩')
  try {
    const distDir = path.resolve(process.cwd(), `${config.publicPath}`)
    const distZipPath = path.resolve(process.cwd(), `${config.publicPath}.zip`)
    await compressing.zip.compressDir(distDir, distZipPath)
    successLog('压缩成功!')
  } catch (error) {
    errorLog(error)
    errorLog('压缩失败, 退出程序!')

    await clearDistZip(config)

    process.exit() // 退出流程
  }
}

module.exports = dist2zip
