const shell = require('shelljs')
const { defaultLog, successLog, errorLog } = require('../utils')

const compile = async (config) => {
  if (config.script) {
    defaultLog('项目开始打包')
  
    shell.cd(process.cwd())
    const res = await shell.exec(`${config.script}`) // 执行shell 打包命令
  
    if (res.code === 0) {
      successLog('项目打包成功!')
    } else {
      errorLog('项目打包失败, 请重试!')
      process.exit()
    }
  }
}

module.exports = compile
