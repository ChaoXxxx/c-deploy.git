#!/usr/bin/env node

const help = require('../lib/help')
const init = require('../lib/init')
const { errorLog, hasConfigFile } = require('../utils')

const www = async () => {
  const argv = process.argv.slice(2)

  if (argv.some(x => ['-h', 'help'].includes(x))) {
    help()
    return
  }

  if (argv.includes('init')) {
    init()
    return
  }

  if (hasConfigFile()) {
    
    const environment = require('../lib/environment')
    const compile = require('../lib/compile')
    const dist2zip = require('../lib/dist2zip')
    const connect = require('../lib/connect')

    const config = await environment()

    if (!argv.includes('upload')) {
      await compile(config)
    }

    await dist2zip(config)
    await connect(config)

    process.exit()
  } else {
    errorLog(`缺少配置文件,自动为你生成配置文件,请修改后再运行`)
    init()
    process.exit()
  }
}

www()
