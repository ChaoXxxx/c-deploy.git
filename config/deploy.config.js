module.exports = [
  // 一个对象为一个环境，多个环境将会询问部署环境，单环境会跳过询问
  {
    name: '测试环境dev', // 部署环境的名称（多环境名称不能重复）
    publicPath: 'dist', // 项目打包之后的文件夹名称，一般都是dist文件夹，如果你的项目打包成别的文件夹名称，填写打包之后文件夹名称即可
    script: 'npm run build', // 打包命令，不需要填空
    host: '', // 服务器ip
    port: '22', // 服务器连接端口通常不用改
    username: 'root', // 部署服务器的账号
    password: '', // 部署服务器的密码，如果重要，可以不写在当前配置文件中(为空运行时将要求输入密码)
    privateKey: '', // 部署服务器的密码私钥，填写私钥路径(从进程执行时的工作目录开始) 优先级 私钥 -> 密码 例:'/home/steel/.ssh/id_rsa' 不填写则寻找密码password
    servicePath: '', // 前端代码在服务器下的路径
    backupDist: true // 是否保留旧版本,为true后会对旧版本publicPath重命名(带时间)进行备份, 类型:Boolean true(当前目录进行备份) false(不备份), String(指定服务器目录进行备份)
  },
  {
    name: '正式环境pro', // 部署环境的名称（多环境名称不能重复）
    publicPath: 'dist', // 项目打包之后的文件夹名称，一般都是dist文件夹，如果你的项目打包成别的文件夹名称，填写打包之后文件夹名称即可
    script: 'npm run build', // 打包命令，不需要填空
    host: '', // 服务器ip
    port: '22', // 服务器连接端口通常不用改
    username: 'root', // 部署服务器的账号
    password: '', // 部署服务器的密码，如果重要，可以不写在当前配置文件中(为空运行时将要求输入密码)
    privateKey: '', // 部署服务器的密码私钥，填写私钥路径(从进程执行时的工作目录开始) 优先级 私钥 -> 密码 例:'/home/steel/.ssh/id_rsa' 不填写则寻找密码password
    servicePath: '', // 前端代码在服务器下的路径
    backupDist: true // 是否保留旧版本,为true后会对旧版本publicPath重命名(带时间)进行备份, 类型:Boolean true(当前目录进行备份) false(不备份), String(指定服务器目录进行备份)
  }
]
